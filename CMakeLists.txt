project(statSem)

set(CMAKE_CXX_STANDARD 14)

add_executable(statSem main.cpp token.cpp token.h testTree.cpp testTree.h scanner.cpp scanner.h parser.cpp parser.h node.h semantics.cpp semantics.h)
