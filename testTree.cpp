//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#include <string>
#include <iostream>
#include "testTree.h"

using namespace std;

/// preorder function to traverse the tree
/// NOTE MOST OF THIS CODE CAME FROM PREORDER TRAVERSAL IN ALOGITHMS BOOK
/// \param node
/// \param level
void preorder(node_t *node, int level){

    //check if node is null. If null break
    if(node == NULL){
        return;
    }
    //line of the tree
    string current_line;

    //Format the tree to have 2:3 ratio of spaces to levels
    for(int i = 0; i < level; i++){
        current_line.append("  ");
    }

    //node label
    current_line.append(node -> label);
    current_line.append("  ");

    //Tokens
    for(int i = 0; i < node->tokens.size(); i++){
        //token des
        current_line.append(node->tokens[i].tk_Description);

        //seperate by commas
        if(((i+1) != node->tokens.size() && (node->tokens[i].ID != OPtk))){
            current_line.append(" ");
        }
        current_line.append(" ");
    }

    //print the current_line
    cout << current_line << "\n";

    //Children printing
    preorder(node->child1, level+1);
    preorder(node->child2, level+1);
    preorder(node->child3, level+1);
    preorder(node->child4, level+1);

}