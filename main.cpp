//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "token.h"
#include "node.h"
#include "scanner.h"
#include "testTree.h"
#include "parser.h"
#include "semantics.h"

using namespace std;

/// Main function for P2
/// \param argc
/// \param argv
/// \return
int main(int argc, char* argv[]){

    //file stream
    ifstream input_file;
    //Input file
    char *input_file_name;

    //argument count passed on terminal. if 2 use filename provided for test file.
    if(argc == 2){
        string fileNamePassed = argv[1];
        cout<< "Using test file: " + fileNamePassed + "\n";

        //copy contents to new file to perserve orginal
        input_file_name = new char[fileNamePassed.length() + 1];
        strcpy(input_file_name, fileNamePassed.c_str());
        //Open the new file
        input_file.open(input_file_name, ifstream::in);
        //Check if successful open
        if(!input_file.is_open()){
            cout<< "ERROR: file" + fileNamePassed + "could not be opened. Check for security issues\n";
            return -1;
        }
        else{
            cout<<"PROGRESS: The file " << input_file_name << " has been opened. Continuing.....\n";
            readFile(input_file);
        }
    }
    else{
        cout<< "ERROR:Please follow the format for Invocation: frontEnd file\n";
    }


    //Call the parser
    node_t* root = parser();
    //call the print of the parser tree
    preorder(root, root->level);

    //call the semantics
    semantics(root);

    //close open files
    input_file.close();

    return 0;
}


